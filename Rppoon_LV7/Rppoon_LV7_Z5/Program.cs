﻿using System;
using System.Collections.Generic;

namespace Rppoon_LV7_Z5
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 5.
            //U primjeru 3 dodati klasu koja predstavlja knjigu koja ima atribute za naslov i cijenu. Napraviti odgovarajuću
            //doradu sučelja i konkretne klase posjetitelja. Testirati primjer

            DVD dvd1 = new DVD("Train to Busan", DVDType.MOVIE,29.99);
            DVD dvd2 = new DVD("Software", DVDType.SOFTWARE, 15);
            VHS vhs = new VHS("Lion King",10);
            Book book = new Book("Call Me By Your Name", 69.99);

            IVisitor visitor = new BuyVisitor();

            visitor.Visit(dvd1);
            Console.WriteLine(dvd1.ToString());
            Console.WriteLine("Price with tax: " + Math.Round(dvd1.Accept(visitor), 2));

            visitor.Visit(dvd2);
            Console.WriteLine(dvd2.ToString());
            Console.WriteLine("Price with tax: " + Math.Round(dvd2.Accept(visitor), 2));

            visitor.Visit(vhs);
            Console.WriteLine(vhs.ToString());
            Console.WriteLine("Price with tax: " + Math.Round(vhs.Accept(visitor), 2));

            visitor.Visit(book);
            Console.WriteLine(book.ToString());
            Console.WriteLine("Price with tax: " + Math.Round(book.Accept(visitor), 2));


        }
    }
}
