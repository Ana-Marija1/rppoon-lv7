﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV7_Z5
{
    class BuyVisitor:IVisitor
    {
        private const double DVDTax = 0.18;
        private const double VHSTax = 0.10;
        private const double bookTax = 0.25;
        public double Visit(DVD DVDItem)
        {
            return DVDItem.Price * (1 + DVDTax);
        }
        //Promjenila sam DVDTax u VHSTax pod pretpostavkom da se dogodila greška u pisanju primjera
        public double Visit(VHS VHSItem)
        {
            return VHSItem.Price * (1 + VHSTax);
        }
        public double Visit(Book bookItem)
        {
            return bookItem.Price * (1 + bookTax);
        }
    }
}
