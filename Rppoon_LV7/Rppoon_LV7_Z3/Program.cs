﻿using System;

namespace Rppoon_LV7_Z3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 3.
            //U primjeru 2 implementirati metode kojima nedostaje implementacija.Potom, testirati primjer. Za testiranje
            //unutar beskonačne petlje dohvaćati trenutno stanje resursa.Na kraju svake iteracije napraviti pauzu od jedne
            //sekunde(koristiti: System.Threading.Thread.Sleep(1000);)

            SystemDataProvider systemDataProvider = new SystemDataProvider();

            string filePath = "D:\\Ana-Marija\\Faks\\IV. semestar\\RPPOON\\Rppoon_LV7\\Rppoon_LV7_Z3\\logFile.txt";
            FileLogger fileLogger = new FileLogger(filePath);
            ConsoleLogger consoleLogger = new ConsoleLogger();
            systemDataProvider.Attach(fileLogger);
            systemDataProvider.Attach(consoleLogger);

            while (true)
            {
                systemDataProvider.GetAvailableRAM();
                systemDataProvider.GetCPULoad();
                System.Threading.Thread.Sleep(1000);
            }
            
        }
    }
}
