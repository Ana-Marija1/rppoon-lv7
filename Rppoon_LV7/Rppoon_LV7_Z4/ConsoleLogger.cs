﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV7_Z4
{
    class ConsoleLogger:Logger
    {
        public void Log(SimpleSystemDataProvider provider)
        {
            //implementation missing
            Console.WriteLine(DateTime.Now + "-> CPU load: " + provider.CPULoad
                + " Available RAM: " + provider.AvailableRAM);
        }
    }
}
