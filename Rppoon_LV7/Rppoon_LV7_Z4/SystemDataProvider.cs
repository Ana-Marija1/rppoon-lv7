﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV7_Z4
{
    class SystemDataProvider:SimpleSystemDataProvider
    {
        private float previousCPULoad;
        private float previousRAMAvailable;
        public SystemDataProvider() : base()
        {
            this.previousCPULoad = this.CPULoad;
            this.previousRAMAvailable = this.AvailableRAM;
        }
        public float GetCPULoad()
        {
            float currentLoad = this.CPULoad;
            if (currentLoad != this.previousCPULoad)
            {
                this.Notify();
            }
            this.previousCPULoad = currentLoad;
            return currentLoad;
        }
        public float GetAvailableRAM()
        {
            // implementation missing
            float currentlyAvailableRAM = this.AvailableRAM;
            if(currentlyAvailableRAM>previousRAMAvailable && currentlyAvailableRAM / previousRAMAvailable < 1.1)
            {
                this.Notify();
            }
            else if (currentlyAvailableRAM<previousRAMAvailable && currentlyAvailableRAM / previousRAMAvailable < 0.9)
            {
                this.Notify();
            }
            this.previousRAMAvailable = currentlyAvailableRAM;
            return currentlyAvailableRAM;
        }
    }
}
