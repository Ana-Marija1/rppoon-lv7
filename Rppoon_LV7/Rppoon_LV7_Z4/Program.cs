﻿using System;

namespace Rppoon_LV7_Z4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 4.
            //Izmijeniti primjer tako da se vrši obavještavanje samo ukoliko je razlika između prethodnog i trenutnog
            //stanja barem 10 %

            SystemDataProvider systemDataProvider = new SystemDataProvider();

            string filePath = "D:\\Ana-Marija\\Faks\\IV. semestar\\RPPOON\\Rppoon_LV7\\Rppoon_LV7_Z4\\logFile.txt";
            FileLogger fileLogger = new FileLogger(filePath);
            ConsoleLogger consoleLogger = new ConsoleLogger();
            systemDataProvider.Attach(fileLogger);
            systemDataProvider.Attach(consoleLogger);

            while (true)
            {
                systemDataProvider.GetAvailableRAM();
                systemDataProvider.GetCPULoad();
                System.Threading.Thread.Sleep(1000);
            }
        }
    }
}
