﻿using System;

namespace Rppoon_LV7_Z7
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 7.
            //Dodati klasu Cart koja predstavlja kolekciju/ agregaciju stavki iz primjera.Ona omogućuje ubacivanje i
            //uklanjanje stavki. Također, i ona pruža mogućnost korištenja posjetitelja. Njena metoda Accept() redom
            //poziva istu na svim stavkama koje drži te vraća zbroj.Izmijeniti posjetitelja za izračun cijene iznajmljivanja
            //tako da cijena iznajmljivanja programske podrške bude jednaka cijeni prodaje.

            DVD dvd1 = new DVD("Train to Busan", DVDType.MOVIE, 29.99);
            DVD dvd2 = new DVD("Software", DVDType.SOFTWARE, 15);
            VHS vhs = new VHS("Lion King", 10);
            Book book = new Book("Call Me By Your Name", 69.99);

            Cart cart = new Cart();
            cart.AddItem(dvd1);
            cart.AddItem(dvd2);
            cart.AddItem(vhs);
            cart.AddItem(book);

            IVisitor visitor = new LendVisitor();

            cart.Accept(visitor);
            Console.WriteLine("Price of items in the cart: " + Math.Round(cart.Accept(visitor), 2));

            cart.RemoveItem(dvd2);
            Console.WriteLine("Price of items in the cart: " + Math.Round(cart.Accept(visitor), 2));

            cart.Clear();
            Console.WriteLine("Price of items in the cart: " + Math.Round(cart.Accept(visitor), 2));
        }
    }
}
