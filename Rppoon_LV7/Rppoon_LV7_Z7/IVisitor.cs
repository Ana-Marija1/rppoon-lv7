﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV7_Z7
{
    interface IVisitor
    {
        double Visit(DVD DVDItem);
        double Visit(VHS VHSItem);
        double Visit(Book bookItem);
    }
}
