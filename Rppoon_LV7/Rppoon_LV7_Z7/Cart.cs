﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV7_Z7
{
    class Cart:IItem
    {
        private List<IItem> items;
        public Cart()
        {
            this.items = new List<IItem>();
        }
        public void AddItem(IItem item)
        {
            this.items.Add(item);
        }
        public void RemoveItem(IItem item)
        {
            this.items.Remove(item);
        }
        public void Clear()
        {
            this.items.Clear();
        }
        public double Accept(IVisitor visitor)
        {
            double price = 0;

            foreach(IItem item in this.items)
            {
                price += item.Accept(visitor);
                
            }
            return price;
        }

    }
}
