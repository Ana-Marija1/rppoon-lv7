﻿using System;

namespace Rppoon_LV7_Z6
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 6.
            //U primjeru 3 napisati posjetitelja za potrebe izračuna cijene iznajmljivanja stavki(DVD, VHS i knjiga). Cijena
            //iznajmljivanja je jednaka 10 % cijene stavke.Uz to, DVD-ove koji su programska podrška nije moguće
            //iznajmiti – vratiti u tom slučaju NaN(not a number) vrijednost.


            DVD dvd1 = new DVD("Train to Busan", DVDType.MOVIE, 29.99);
            DVD dvd2 = new DVD("Software", DVDType.SOFTWARE, 15);
            VHS vhs = new VHS("Lion King", 10);
            Book book = new Book("Call Me By Your Name", 69.99);

            IVisitor visitor = new LendVisitor();

            visitor.Visit(dvd1);
            Console.WriteLine(dvd1.ToString());
            Console.WriteLine("Price of lending: " + Math.Round(dvd1.Accept(visitor), 2));

            visitor.Visit(dvd2);
            Console.WriteLine(dvd2.ToString());
            Console.WriteLine("Price of lending: " + Math.Round(dvd2.Accept(visitor), 2));

            visitor.Visit(vhs);
            Console.WriteLine(vhs.ToString());
            Console.WriteLine("Price of lending: " + Math.Round(vhs.Accept(visitor), 2));

            visitor.Visit(book);
            Console.WriteLine(book.ToString());
            Console.WriteLine("Price of lending: " + Math.Round(book.Accept(visitor), 2));

        }
    }
}
