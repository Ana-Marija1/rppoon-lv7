﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV7_Z6
{
    class LendVisitor:IVisitor
    {
        private const double DVDTax = 0.18;
        private const double VHSTax = 0.10;
        private const double bookTax = 0.25;
        public double Visit(DVD DVDItem)
        {
            //Stavila sam 10% ukupne cijene (uračunat porez)

            if (DVDItem.Type == DVDType.SOFTWARE)
            {
                return double.NaN;
            }
            return (DVDItem.Price * (1 + DVDTax))*0.1;
        }
        //Promjenila sam DVDTax u VHSTax pod pretpostavkom da se dogodila greška u pisanju primjera
        public double Visit(VHS VHSItem)
        {
            return (VHSItem.Price * (1 + VHSTax))*0.1;
        }
        public double Visit(Book bookItem)
        {
            return (bookItem.Price * (1 + bookTax))*0.1;
        }
    }
}
