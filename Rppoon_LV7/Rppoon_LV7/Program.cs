﻿using System;

namespace Rppoon_LV7_Z1_2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 1.
            //U primjeru 1 implementirati metodu kojoj nedostaje implementacija u klasi BubbleSort. Potom, testirati
            //primjer.

            double[] array = new[] { 1.62, 9.78, 9.78, 1.08, 2.75, 5.96, 3.38, 2.88, 7.71, 2.34, 7.37, 3.32, 8.37 };
            NumberSequence numberSequence1 = new NumberSequence(array);

            numberSequence1.SetSortStrategy(new SequentialSort());
            numberSequence1.Sort();
            Console.WriteLine("Sequential sort:");
            Console.WriteLine(numberSequence1.ToString());

            numberSequence1.SetSortStrategy(new CombSort());
            numberSequence1.Sort();
            Console.WriteLine("Comb sort:");
            Console.WriteLine(numberSequence1.ToString());

            numberSequence1.SetSortStrategy(new BubbleSort());
            numberSequence1.Sort();
            Console.WriteLine("Bubble sort:");
            Console.WriteLine(numberSequence1.ToString());

            //Zadatak 2.
            //Po uzoru na primjer 1 ostvariti algoritam za pretraživanje(primjerice, slijedno/ linearno pretraživanje).
            //Uvesti i odgovarajuće sučelje strategije.Proširiti klasu NumberSequence tako da omogućuje i pretraživanje
            //slijeda brojeva preko nove klase.

            NumberSequence numberSequence2 = new NumberSequence(10);

            Random random = new Random();
            double randNumber;
            for(int i = 0; i < 10; i++)
            {
                randNumber = Math.Round(1 + (10 - 1 )* random.NextDouble(),2);
                numberSequence2.InsertAt(i, randNumber);
                
                //Za provjeru:
                //Console.WriteLine(randNumber);
            }

            Console.WriteLine("Enter number to search for:");
            double searchedNumber = Convert.ToDouble(Console.ReadLine());

            numberSequence2.SetSearchAlgorithm(new LinearSearch());
            int integer1 = numberSequence2.Search(searchedNumber);
            if (integer1 == -1)
            {
                Console.WriteLine("Searched number was not found.");
            }
            else
            {
                Console.WriteLine("Searched number was found in place {0} in array", integer1 + 1);
            }

            //For binary search array needs to be sorted.

            numberSequence2.SetSortStrategy(new CombSort());
            numberSequence2.Sort();
            numberSequence2.SetSearchAlgorithm(new BinarySearch());
            int integer2 = numberSequence2.Search(searchedNumber);
            if (integer2 == -1)
            {
                Console.WriteLine("Searched number was not found.");
            }
            else
            {
                Console.WriteLine("Searched number was found in place {0} in array", integer2 + 1);
            }
        }
    }
}
