﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV7_Z1_2
{
    class NumberSequence
    {
        private double[] sequence;
        private int sequenceSize;
        private SortStrategy sortStrategy;
        private ISearchAlgoritmh searchAlgoritmh;
        public NumberSequence(int sequenceSize)
        {
            this.sequenceSize = sequenceSize;
            this.sequence = new double[sequenceSize];
        }
        public NumberSequence(double[] array) : this(array.Length)
        {
            array.CopyTo(this.sequence, 0);
        }
        public void InsertAt(int index, double value)
        {
            this.sequence[index] = value;
        }
        public void SetSortStrategy(SortStrategy strategy)
        {
            this.sortStrategy = strategy;
        }
        public void Sort() { this.sortStrategy.Sort(this.sequence); }
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            foreach (double element in this.sequence)
            {
                builder.Append(element).Append(Environment.NewLine);
            }
            return builder.ToString();
        }

        public void SetSearchAlgorithm(ISearchAlgoritmh searchAlgoritmh)
        {
            this.searchAlgoritmh = searchAlgoritmh;
        }
        public int Search(double searchedNumber)
        {
            return this.searchAlgoritmh.Search(this.sequence, searchedNumber);
        }
    }
}
