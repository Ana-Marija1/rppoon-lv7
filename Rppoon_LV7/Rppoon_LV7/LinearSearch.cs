﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV7_Z1_2
{
    class LinearSearch:ISearchAlgoritmh
    {
        public int Search(double[] array, double searchedValue)
        {
            int arraySize = array.Length;
            for (int i = 0; i < arraySize; i++)
            {
                if (array[i] == searchedValue)
                {
                    return i;
                }
            }
            return -1;
        }
    }
}
