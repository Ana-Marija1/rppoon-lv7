﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV7_Z1_2
{
    class BinarySearch:ISearchAlgoritmh
    {
        public int Search(double[] array, double searchedValue)
        {
            int arraySize = array.Length;
            int lowerLimit = 0;
            int upperLimit = arraySize - 1;
            int middle = (lowerLimit + upperLimit) / 2;

            while (lowerLimit <= upperLimit)
            {
                if (array[middle] == searchedValue)
                {
                    return middle;
                }
                else if (array[middle] < searchedValue)
                {
                    lowerLimit = middle + 1;
                }
                else if(array[middle] > searchedValue)
                {
                    upperLimit = middle - 1;
                }
                middle = (lowerLimit + upperLimit) / 2;
            }
            return -1;
        }
    }
}
